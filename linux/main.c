#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "morseme.h"

// --- MAIN --- //
int main(int argc, char **argv) {
    int   i;
    int   ti      = 200000;
    char *message = NULL;
    int   opt;
    int   caseState = -1;

    while ((opt = getopt(argc, argv, "m:")) != -1) {
        switch (opt) {
        case 'm':
            message = optarg;
            break;

        default:
            printf("Usage : %s [-m <message>]\n", argv[0]);
            exit(EXIT_FAILURE);
            break;
        }
    }

    // IF MESSAGE HAS NOT BEEN INITIALISED (HENCE NO -m ARGUMENT), DISPLAY USAGE MESSAGE AND EXIT
    if (!(message)) {
        printf("Usage : %s [-m <message>]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("\e[33m>> Running...\e[0m\n\n");

    for (i = 0; i < strlen(message); i++) {
        // CHECK IF THE LETTER IS LOWERCASE OR UPPERCASE OR NEITHER
        caseState = checkCase(message[i]);

        if (caseState == -1) {
            // IF THE CURRENT LETTER IS NOT A LEGITIMATE LETTER, DISPLAY ERROR AND EXIT
            printf("Special characters are not supported yet !\n");
            exit(EXIT_FAILURE);
        }else if ((caseState)) {
            // IF THE LETTER IS UPPERCASE, DOWNGRADE IT TO LOWERCASE IN ORDER TO EASE THE MORSEME FUNCTION
            message[i] += 32;
        }

        if (message[i] == 32) { // IF SPACE (ASCII 32)
            usleep(ti * 6);     // SEVEN TI BETWEEN WORDS MINUS THE ONE THAT GET DONE AFTER EACH LETTERS
        }
        printf("%c\n", message[i]);
        morseMe(message[i]); // PARSE THE ARRAY AND DISPLAY THE MORSE SIGNAL LETTER BY LETTER (ONE LOOP = ONE LETTER)
        usleep(ti);          // ONE TI BETWEEN EACH SIGNALS
    }

    printf("\n\e[32m>> Done\e[0m\n");

    return 0;
}

// --- FUNCTIONS --- //
