#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int checkCase(char letter) {
    short caseType;

    if (letter >= 65 && letter <= 90) {        // UPPERCASE
        caseType = 1;
    }else if (letter >= 97 && letter <= 122) { // LOWERCASE
        caseType = 0;
    }else{                                     // NEITHER
        caseType = -1;
    }

    return caseType;
}

void blink(int blinkType) {
    /*
    ** REMINDER
    **
    ** >> "usleep" uses micro seconds
    **
    ** >> One TA equals three TI (1*TA=3*TI)
    ** >> TA = 0,60 second <=> 600 000 micro seconds
    ** >> TI = 0,20 second <=> 200 000 micro seconds
    **
    ** >> 1 TA between each letters
    ** >> 3 TI between each words
    */

    if (blinkType == 0) { // TI
        system("xdotool key -delay=200 66 66"); // BLINK 200ms
    }else if (blinkType == 1)  { // TA
        system("xdotool key -delay=600 66 66"); // BLINK 600ms
    }
}

void morseMe(char letter) {
    switch (letter) {
    case 'a':
        blink(0);
        blink(1);
        break;

    case 'b':
        blink(1);
        blink(0);
        blink(0);
        blink(0);
        break;

    case 'c':
        blink(0);
        blink(1);
        blink(0);
        blink(1);
        break;

    case 'd':
        blink(1);
        blink(0);
        blink(0);
        break;

    case 'e':
        blink(0);
        break;

    case 'f':
        blink(0);
        blink(0);
        blink(1);
        blink(0);
        break;

    case 'g':
        blink(1);
        blink(1);
        blink(0);
        break;

    case 'h':
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        break;

    case 'i':
        blink(0);
        blink(0);
        break;

    case 'j':
        blink(0);
        blink(1);
        blink(1);
        blink(1);
        break;

    case 'k':
        blink(1);
        blink(0);
        blink(1);
        break;

    case 'l':
        blink(0);
        blink(1);
        blink(0);
        blink(0);
        break;

    case 'm':
        blink(1);
        blink(1);
        break;

    case 'n':
        blink(1);
        blink(0);
        break;

    case 'o':
        blink(1);
        blink(1);
        blink(1);
        break;

    case 'p':
        blink(0);
        blink(1);
        blink(1);
        blink(0);
        break;

    case 'q':
        blink(1);
        blink(0);
        blink(1);
        blink(1);
        break;

    case 'r':
        blink(0);
        blink(1);
        blink(0);
        break;

    case 's':
        blink(0);
        blink(0);
        blink(0);
        break;

    case 't':
        blink(1);
        break;

    case 'u':
        blink(0);
        blink(0);
        blink(1);
        break;

    case 'v':
        blink(0);
        blink(0);
        blink(0);
        blink(1);
        break;

    case 'w':
        blink(0);
        blink(1);
        blink(1);
        break;

    case 'x':
        blink(1);
        blink(0);
        blink(0);
        blink(1);
        break;

    case 'y':
        blink(1);
        blink(0);
        blink(1);
        blink(1);
        break;

    case 'z':
        blink(1);
        blink(1);
        blink(0);
        blink(0);
        break;

    case '1':
        blink(0);
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        break;

    case '2':
        blink(0);
        blink(0);
        blink(1);
        blink(1);
        blink(1);
        break;

    case '3':
        blink(0);
        blink(0);
        blink(0);
        blink(1);
        blink(1);
        break;

    case '4':
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        blink(1);
        break;

    case '5':
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        break;

    case '6':
        blink(1);
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        break;

    case '7':
        blink(1);
        blink(1);
        blink(0);
        blink(0);
        blink(0);
        break;

    case '8':
        blink(1);
        blink(1);
        blink(1);
        blink(0);
        blink(0);
        break;

    case '9':
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        blink(0);
        break;

    case '0':
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        break;
    }
}
