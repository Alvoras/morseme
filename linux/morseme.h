#ifndef MORSEME_H_INCLUDED
#define MORSEME_H_INCLUDED

int checkCase(char letter);
void blink(int blinkType);
void morseMe(char letter);

#endif // MORSEME_H_INCLUDED
