#define WINVER 0x0500 // SendInput only works with Windows2000 and later, so WINVER needs to be set as such to get defined with windows.h included below
#include <stdio.h>
#include <windows.h>

int main(int argc, char **argv)
{
    char food[140];
    int i;
    int lim;
    char buffer;

    printf("String to display :\n");
    gets(food); // input string

    printf(">> Running...\n");

    for(i = 0; i < strlen(food); i++)
    {
        morseMe(food[i]); // parse the array and display the morse signal letter by letter (one loop = one letter)
    }

    printf("\n>> Done.");

    return 0;
}
void blink(int blinkType)
{
    /*
    ** REMINDER
    **
    ** >> usleep uses  micro seconds
    **
    ** >> One TA equals three TI (1*TA=3*TI)
    ** >> TA = 0,60 second / 600 000 micro seconds
    ** >> TI = 0,20 second / 200 000 micro seconds
    **
    ** >> One TA between each letters
    ** >> Seven TI between each words
    **
    ** END OF REMINDER
    */

    INPUT ip;

    if (blinkType == 0)
    {
        // set up keyboard event
        ip.type = INPUT_KEYBOARD;
        ip.ki.wScan = 0; // hardware scan code for key
        ip.ki.time = 0;
        ip.ki.dwExtraInfo = 0;

        usleep(450000); // one TA between each letters

        // key down
        ip.ki.wVk = 0x14; // Capslock
        ip.ki.dwFlags = 0; // 0 for key press
        SendInput(1, &ip, sizeof(INPUT));

        // key up
        ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
        SendInput(1, &ip, sizeof(INPUT));

        usleep(150000); // TI duration

        // push the key again so the light goes out
        ip.ki.wVk = 0x14; // Capslock
        ip.ki.dwFlags = 0; // 0 for key press
        SendInput(1, &ip, sizeof(INPUT));

        // key up ; one blink done
        ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
        SendInput(1, &ip, sizeof(INPUT));

        usleep(150000); // one TI between each signals

    }
    else if (blinkType == 1)
    {
        // Set up a generic keyboard event.
        ip.type = INPUT_KEYBOARD;
        ip.ki.wScan = 0; // hardware scan code for key
        ip.ki.time = 0;
        ip.ki.dwExtraInfo = 0;

        usleep(450000); // one TA between each letters

        // key down
        ip.ki.wVk = 0x14; // Capslock
        ip.ki.dwFlags = 0; // 0 for key press
        SendInput(1, &ip, sizeof(INPUT));

        // key up
        ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
        SendInput(1, &ip, sizeof(INPUT));

        usleep(450000); // TA duration

        // push the key again so the light goes out
        ip.ki.wVk = 0x14; // Capslock
        ip.ki.dwFlags = 0; // 0 for key press
        SendInput(1, &ip, sizeof(INPUT));

        // key up ; one blink done
        ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
        SendInput(1, &ip, sizeof(INPUT));

        usleep(150000); // one TI between each signals

    }
}
void morseMe(char letter)
{
    switch (letter)
    {
    case 'a':
        blink(0);
        blink(1);
        break;
    case 'b':
        blink(1);
        blink(0);
        blink(0);
        blink(0);
        break;
    case 'c':
        blink(0);
        blink(1);
        blink(0);
        blink(1);
        break;
    case 'd':
        blink(1);
        blink(0);
        blink(0);
        break;
    case 'e':
        blink(0);
        break;
    case 'f':
        blink(0);
        blink(0);
        blink(1);
        blink(0);
        break;
    case 'g':
        blink(1);
        blink(1);
        blink(0);
        break;
    case 'h':
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        break;
    case 'i':
        blink(0);
        blink(0);
        break;
    case 'j':
        blink(0);
        blink(1);
        blink(1);
        blink(1);
        break;
    case 'k':
        blink(1);
        blink(0);
        blink(1);
        break;
    case 'l':
        blink(0);
        blink(1);
        blink(0);
        blink(0);
        break;
    case 'm':
        blink(1);
        blink(1);
        break;
    case 'n':
        blink(1);
        blink(0);
        break;
    case 'o':
        blink(1);
        blink(1);
        blink(1);
        break;
    case 'p':
        blink(0);
        blink(1);
        blink(1);
        blink(0);
        break;
    case 'q':
        blink(1);
        blink(0);
        blink(1);
        blink(1);
        break;
    case 'r':
        blink(0);
        blink(1);
        blink(0);
        break;
    case 's':
        blink(0);
        blink(0);
        blink(0);
        break;
    case 't':
        blink(1);
        break;
    case 'u':
        blink(0);
        blink(0);
        blink(1);
        break;
    case 'v':
        blink(0);
        blink(0);
        blink(0);
        blink(1);
        break;
    case 'w':
        blink(0);
        blink(1);
        blink(1);
        break;
    case 'x':
        blink(1);
        blink(0);
        blink(0);
        blink(1);
        break;
    case 'y':
        blink(1);
        blink(0);
        blink(1);
        blink(1);
        break;
    case 'z':
        blink(1);
        blink(1);
        blink(0);
        blink(0);
        break;
    case '1':
        blink(0);
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        break;
    case '2':
        blink(0);
        blink(0);
        blink(1);
        blink(1);
        blink(1);
        break;
    case '3':
        blink(0);
        blink(0);
        blink(0);
        blink(1);
        blink(1);
        break;
    case '4':
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        blink(1);
        break;
    case '5':
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        break;
    case '6':
        blink(1);
        blink(0);
        blink(0);
        blink(0);
        blink(0);
        break;
    case '7':
        blink(1);
        blink(1);
        blink(0);
        blink(0);
        blink(0);
        break;
    case '8':
        blink(1);
        blink(1);
        blink(1);
        blink(0);
        blink(0);
        break;
    case '9':
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        blink(0);
        break;
    case '0':
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        blink(1);
        break;
    case ' ':
        usleep(1350000);
        break;
    }
}
